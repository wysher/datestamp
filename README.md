datestamp is a immutable date library, which can be nicely described by the Merriam-Webster definition:

```
date stamp - the date and related information stamped by a date stamp
```

And this is what this library is. `datestamp` retruns an object with date, related informations and methods to manipulate and compare dates. Each manipulation method returns new object, which means that you don't need to worry about clonning. You can also banish confusion about first day of week or first month of year. It's `1` with `datestamp`.

I wanted this library to be simple, so no localization is included.

## install
#### npm:

```js
npm i datestamp
```

#### yarn:

```js
yarn add datestamp
```

## Usage

```js
import datestamp from 'datestamp';

const dateTime = datestamp(); // now
```

as argument for datestamp method you can pass:

* Date

```js
const dateTime = datestamp(new Date()); // now
```
* Array

```js
const dateTime = datestamp([2018, 1, 20, 10, 14, 30]); // 20.01.2018 14:30
const dateTime = datestamp([2018]); // 01.01.2018 00:00
// note that month starts with 1 not 0.
```


* Object

```js
const dateTime = datestamp({ year: 2018, month: 1, day: 20, hour: 14, minute: 30 }); // 20.01.2018 14:30
const dateTime = datestamp({  year: 2018 }); // 01.01.2018 00:00

// with aliasses
const dateTime = datestamp({ Y: 2018, M: 1, D: 20, H: 14, m: 30 }); // 20.01.2018 14:30

// note that month starts with 1 not 0.
// below you can find list of all alliasses
```

## Datestamp object
Calling datestamp will return datestamp object with helper methods and unit getters listed below:

| Unit | Description |
| :---: |:---:|
| millisecond | |
| second | |
| minute | |
| hour | |
| day | day of month |
| month | starting with 1 |
| year | |
| unix | value of date |
| date | js Date instance |

```js
const dt = datestamp({ year: 2018, month: 1, day: 20, hour: 14, minute: 30 }); // 20.01.2018 14:30

console.log(dt)
/*
{
      date: 2018-01-20T13:30:00.000Z,
      millisecond: 0,
      second: 0,
      minute: 30,
      hour: 14,
      day: 20,
      week: 3,
      month: 1,
      year: 2018,
      unix: 1516455000000,
      weekday: 2,
      dayOfYear: 20
}
*/

dt.year // 2018
```

## Getters
There are only three getters, which are not part of Datestamp object due to performance reasons.

| Getter | Description |
| :---: |:---:|
| weekday | from 1 to 7, where 0 is sunday, and 7 is saturday |
| week | week of year |
| dayOfYear | day of year |

```js
const dt = datestamp({ year: 2018, month: 1, day: 20}); // 20.01.2018

dt.week() // 3
dt.weekday() // 7 (saturday)
dt.dayOfYear() // 20
```

## setting firstDayOfWeek globaly
By default first day of week is 0 (sunday). But there's some localications where first day of week is different.
For this cases you can globally set first day of week like so:

```js
datestamp.firstDayOfWeek(1); // monday is now first day of week
const dt = datestamp({ year: 2018, month: 1, day: 20 }); // 20.01.2018

dt.weekday() // 6 (saturday)
```

## Methods
Methods uses unified set of units with aliases, which means you can use alias you like for all datestamp methods.

### Units List
| Unit | Aliases |
| :---: | :---: |
|year | Y, y, yr, yrs, year, years |
|month | M, mo, mth, mths, month, months |
|week | W, w, week, weeks |
|day | D, d, da, day, days |
|hour | H, h, hr, hrs, hour, hours |
|minute | m, min, mins, minute, minutes |
|second | S, s, sec, second, seconds |
|millisecond | ms, millisecond, milliseconds |

### set
there are two ways to set date
* passing two arguments: unit (string) and value (number)
```js
const dt = datestamp('year', 2018); // 01.01.2018 00:00

const dt1 = dt.set('day', 15); // 15.01.2018 00:00

// with alias
const dt2 = dt.set('d', 15); // 15.01.2018 00:00
```

* passing one argument - object with units and values
```js
const dt = datestamp({ year: 2018 }); // 01.01.2018 00:00

const dt1 = dt.set({ year: 2020, month: 3, day: 20, hour: 10, minute: 15 }); // 20.03.2020 10:15

// with aliasses
const dt2 = dt.set({ Y: 2020, M: 3, D: 20, H: 10, m: 15 }); // 20.03.2020 10:15
```

### clone
returns new datestamp object
```js
const dt = datestamp(); // now
const dt = dt.clone() // cloned dt
```

### add / aubtract
returns new datestamp. As for set method there are two ways to add or subtract date:

* passing two arguments: value (number) and unit (string)
```js
const dt = datestamp({ year: 2018, month: 2, day: 10 }); // 10.02.2018 00:00

const dt1 = dt.add(1, 'day'); // 11.02.2018 00:00
const dt2 = dt.subtract(1, 'month'); // 11.01.2018 00:00

// with alias
const dt3 = dt.add(1, 'd'); // 11.02.2018 00:00
const dt4 = dt.subtract(1, 'M'); // 11.01.2018 00:00
```

* passing one argument - object with units and values
```js
const dt = datestamp({ year: 2018, month: 2, day: 10 }); // 10.02.2018 00:00

const dt1 = dt.add({ year: 1, month: 1, day: 1, hour: 1, minute: 1 }); // 11.03.2019 01:01
const dt2 = dt.subtract({ year: 1, month: 1, day: 1 }); // 09.01.2017 00:00

// with aliasses
const dt3 = dt.add({ Y: 1, M: 1, D: 1, H: 1, m: 1 }); // 11.03.2019 01:01
const dt4 = dt.subtract({ Y: 1, M: 1, D: 1 }); // 09.01.2017 00:00
```

### startOf / endOf
returns new datestamp with start / end of passed unit (string)

```js
const dt = datestamp({ year: 2018, month: 2, day: 10, hour: 12, minute: 30 }); // 10.02.2018 12:30

const dt1 = dt.startOf('year') // 01.01.2018 00:00
const dt2 = dt.endOf('year') // 31.12.2018 23:59

// with alias
const dt1 = dt.startOf('H') // 10.02.2018 12:00
const dt2 = dt.endOf('H') // 10.02.2018 12:59
```

### min / max
returns new datestamp with min or max date

```js
const dt = datestamp({ year: 2018, month: 2, day: 10, hour: 12, minute: 30 }); // 10.02.2018 12:30
const dt2 = datestamp({ year: 2018, month: 2, day: 11, hour: 12, minute: 30 }); // 11.02.2018 12:30

const dtMin = dt.min(dt2) // new datestamp with dt
const dtMax = dt.max(dt2) // new datestamp with dt2
```

### diff
Diffs two datestamp objects (considers DST).
Returns diff value in specific unit (millisecond if unit not passed)

```js
const dt = datestamp({ year: 2018 }); // 01.01.2018 00:00
const dt2 = datestamp({ year: 2019 }); // 01.01.2019 00:00

dt.diff(dt2, 'day') // 365
dt.diff(dt2, 'hour') // 8760
```

## Comparison methods
All methods bellow (isSame, isAfter, isBefore, isSameOrBefore, isSameOrAfter) takes two arguments:

    1. date to compare (datestamp object)
    2. unit ('milliseconds' as default)

And returns **true** or **false**.

### Examples
##### isSame
```js
const a = datestamp({ year: 2018 }); // 01.01.2018 00:00
const b = datestamp({ year: 2018, month: 2 }); // 01.02.2018 00:00

a.isSame(b, 'year') // true
a.isSame(b, 'month') // false
a.isSame(b, 'day') // true
```

##### isAfter / isBefore
```js
const a = datestamp({ year: 2018 }); // 01.01.2018 00:00
const b = datestamp({ year: 2018, month: 2 }); // 01.02.2018 00:00

a.isAfter(b, 'year') // false
a.isAfter(b, 'month') // false
a.isAfter(b, 'day') // false

b.isAfter(a, 'year') // false
b.isAfter(a, 'month') // true
b.isAfter(a, 'day') // true

a.isBefore(b, 'year') // false
a.isBefore(b, 'month') // true
a.isBefore(b, 'day') // true

b.isBefore(a, 'year') // false
b.isBefore(a, 'month') // false
b.isBefore(a, 'day') // false
```

##### isSameOrAfter / isSameOrBefore
```js
const a = datestamp({ year: 2018 }); // 01.01.2018 00:00,
const b = datestamp({ year: 2018, month: 2 }); // 01.02.2018 00:00

a.isSameOrAfter(b, 'year') // true
a.isSameOrAfter(b, 'month') // false
a.isSameOrAfter(b, 'day') // false

b.isSameOrAfter(a, 'year') // true
b.isSameOrAfter(a, 'month') // true
b.isSameOrAfter(a, 'day') // true

a.isSameOrBefore(b, 'year') // true
a.isSameOrBefore(b, 'month') // true
a.isSameOrBefore(b, 'day') // true

b.isSameOrBefore(a, 'year') // true
b.isSameOrBefore(a, 'month') // false
b.isSameOrBefore(a, 'day') // false
```

### isBetween
works similar to comparison methods above, but takes 3 arguments:
    1. start of date range (datestamp object)
    2. end of date range (datestamp object)
    3. unit ('milliseconds' as default)

#### Example
```js
const dt = datestamp({ year: 2018, month: 6 }); // 01.06.2018 00:00
const a = datestamp({ year: 2018, month: 5 }); // 01.05.2018 00:00
const b = datestamp({ year: 2018, month: 7 }); // 01.07.2018 00:00

dt.isBetween(a, b, 'year') // false
dt.isBetween(a, b, 'month') // true
dt.isBetween(b, a, 'month') // true
dt.isBetween(a, b, 'day') // true
```

## Format
simple formatting date and time (no localization).

Format gets string use simple implementation of String.prototype.replace() and returns unit for recognized pattern.
#### Example usage
```js
const dt = datestamp({ year: 2018, month: 6, day: 1, hour: 8, minute: 5, second: 1, millisecond: 50 });
dt.format('D.M.YYYY H:m:S') // // 1.6.2018 8:5:1
dt.format('YYYY-MM.DD HH:mm:ss') // // 2018-06-01 08:05:01
```
Here's list of all know patterns (for date above)

| pattern | unit | replaced to |
| :---: | :---: | :---: |
| YYYY | year | 2018 |
| MM | month | 06 |
| M | month | 6 |
| DD | day | 01 |
| D | day | 1 |
| HH | hour | 08 |
| H | hour | 8 |
| mm | minute | 05 |
| m | minute | 5 |
| SS | second | 01 |
| S | second | 1 |
| PP | 12'clock suffix | AM |
| pp | 12'clock suffix | am |

There are also three special cases for format method:
    * calling without parameter - `dt.format()` will return same return as Date.toLocaleString,
    * calling with 'date' string - `dt.format('date')` will return same return as Date.toLocaleDateString,
    * calling with 'time' string - `dt.format('date')` will return same return as Date.toLocaleTimeString,

#### TODO:
* [ ] use UTC?
* [ ] add more tests
* [ ] add quarter unit?

## License
Datestamp is [MIT licensed](./LICENSE).
