module.exports = {
    coverageReporters: ['json', 'lcov', 'text-summary'],
    coveragePathIgnorePatterns: ['/node_modules/'],
    moduleDirectories: [
        './',
        'node_modules',
        'src',
    ],
    coverageThreshold: {
        global: {
            statements: 85,
            branches: 85,
            functions: 85,
            lines: 85,
        },
    },
};
