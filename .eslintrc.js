module.exports = {
    "extends": ["airbnb-base/legacy", "plugin:jest/recommended"],
    "plugins": ["import", "jest"],

    "rules": {
        "indent": ["error", 4],
        "no-console": "error",
        "no-confusing-arrow": [0],
        "import/no-extraneous-dependencies": [0],
        "no-underscore-dangle": ["error", {
            "allowAfterThis": true,
            "allowAfterSuper": true
        }],
        "no-unused-expressions": ["error", {
            "allowTernary": true,
        }],
        "comma-dangle": ["error", {
            "functions": "never",
            "arrays": "always-multiline",
            "objects": "always-multiline",
            "imports": "always-multiline",
            "exports": "always-multiline",
        }],
        "max-len": ["error", 120],
        "max-depth": ["error", 4],
        "max-nested-callbacks": ["error", 6],
        "complexity": ["warn", 6],
        "key-spacing": ["error", {
            "multiLine":  { "mode": "minimum" }
        }],
    },
    "parserOptions": {
        "sourceType": "module",
        "ecmaFeatures": {
            "experimentalObjectRestSpread": true
        }
    }
};
