// order matters
export const UNITS = [
    {
        name: 'millisecond',
        set: 'setMilliseconds',
        get: 'getMilliseconds',
        aliases: ['ms', 'millisecond', 'milliseconds'],
    },
    {
        name: 'second',
        set: 'setSeconds',
        get: 'getSeconds',
        aliases: ['S', 's', 'sec', 'second', 'seconds'],
    },
    {
        name: 'minute',
        set: 'setMinutes',
        get: 'getMinutes',
        aliases: ['m', 'min', 'mins', 'minute', 'minutes'],
    },
    {
        name: 'hour',
        set: 'setHours',
        get: 'getHours',
        aliases: ['H', 'h', 'hr', 'hrs', 'hour', 'hours'],
    },
    {
        name: 'day',
        set: 'setDate',
        get: 'getDate',
        aliases: ['D', 'd', 'da', 'day', 'days'],
    },
    {
        name: 'week',
        set: 'setDate',
        get: 'getDate',
        aliases: ['W', 'w', 'week', 'weeks'],
    },
    {
        name: 'month',
        set: 'setMonth',
        get: 'getMonth',
        aliases: ['M', 'mo', 'mth', 'mths', 'month', 'months'],
    },
    {
        name: 'year',
        set: 'setFullYear',
        get: 'getFullYear',
        aliases: ['Y', 'y', 'yr', 'yrs', 'year', 'years'],
    },
];

// without weeks
export const BASIC_UNITS = [...UNITS].filter(({ name }) => name !== 'week');
