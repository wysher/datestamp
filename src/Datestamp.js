import * as utils from './utils';
import * as diff from './diff';
import format from './format';
import { BASIC_UNITS } from './constans';
import { isObject, isDefined, setDateFromObject, isDate } from './utils';

const errorPrefix = 'you should pass datestamp or Date object';

let firstDayOfWeek = 0;
export class Datestamp {
    constructor(date) {
        this.date = utils.constructDate(date);
        BASIC_UNITS.forEach(({ name, get }) => {
            if (name === 'month') {
                this[name] = this.date[get]() + 1;
            } else {
                this[name] = this.date[get]();
            }
        });

        this.unix = +this.date;
    }

    static getValid(d) {
        if (d instanceof Datestamp) {
            return d;
        } else if (isDate(d)) {
            return new Datestamp(d);
        }
        return null;
    }

    week() {
        return utils.getWeekNumber(this.date, firstDayOfWeek);
    }
    weekday() {
        return utils.getWeekday(this.date, firstDayOfWeek);
    }

    dayOfYear() {
        return utils.getDayOfYear(this.date);
    }

    clone() {
        const date = utils.clone(+this.date);
        return new Datestamp(date);
    }

    set(keys, value) {
        const date = utils.clone(this.date);
        if (typeof keys === 'string' && isDefined(value)) {
            setDateFromObject(date, { [keys]: value });
        } else if (isObject(keys)) {
            setDateFromObject(date, keys);
        }
        return new Datestamp(date);
    }

    add(value, unit) {
        const date = utils.clone(this.date);
        if (typeof unit === 'string') {
            utils.add(date, { [unit]: value });
        } else if (isObject(value)) {
            utils.add(date, value);
        }
        return new Datestamp(date);
    }

    subtract(value, unit) {
        const date = utils.clone(this.date);
        if (typeof unit === 'string') {
            utils.add(date, { [unit]: -value });
        } else if (isObject(value)) {
            const subtractObj = Object
                .entries(value)
                .reduce((memo, [key, val]) => ({ ...memo, [key]: -val }), {});
            utils.add(date, subtractObj);
        }
        return new Datestamp(date);
    }

    startOf(unit) {
        const date = utils.startOf(utils.clone(this.date), unit, firstDayOfWeek);
        return new Datestamp(date);
    }

    endOf(unit) {
        const date = utils.endOf(utils.clone(this.date), unit, firstDayOfWeek);
        return new Datestamp(date);
    }

    min(d) {
        const validDate = Datestamp.getValid(d);
        if (validDate) {
            const result = this.unix <= validDate.unix ? this : validDate;
            return new Datestamp(result.date);
        }
        throw new Error(`${errorPrefix} to min method`);
    }

    max(d) {
        const validDate = Datestamp.getValid(d);
        if (validDate) {
            const result = this.unix >= validDate.unix ? this : validDate;
            return new Datestamp(result.date);
        }
        throw new Error(`${errorPrefix} to max method`);
    }

    diff(d, unit) {
        const validDate = Datestamp.getValid(d);
        if (validDate) {
            return Math.floor(diff.getDiff(this.date, validDate.date, unit));
        }
        throw new Error(`${errorPrefix} as first argument of diff method`);
    }

    isAfter(d, unit) {
        const validDate = Datestamp.getValid(d);
        if (validDate) {
            return diff.isAfter(this.date, validDate.date, unit, firstDayOfWeek);
        }
        throw new Error(`${errorPrefix} as first argument of isAfter method`);
    }

    isBefore(d, unit) {
        const validDate = Datestamp.getValid(d);
        if (validDate) {
            return diff.isBefore(this.date, validDate.date, unit, firstDayOfWeek);
        }
        throw new Error(`${errorPrefix} as first argument of isBetween method`);
    }

    isSame(d, unit) {
        const validDate = Datestamp.getValid(d);
        if (validDate) {
            return diff.isSame(this.date, validDate.date, unit, firstDayOfWeek);
        }
        throw new Error(`${errorPrefix} as first argument of isAfter method`);
    }

    isSameOrAfter(d, unit) {
        const validDate = Datestamp.getValid(d);
        if (validDate) {
            return diff.isSameOrAfter(this.date, validDate.date, unit, firstDayOfWeek);
        }
        throw new Error(`${errorPrefix} as first argument of isSameOrAfter method`);
    }

    isSameOrBefore(d, unit) {
        const validDate = Datestamp.getValid(d);
        if (validDate) {
            return diff.isSameOrBefore(this.date, validDate.date, unit, firstDayOfWeek);
        }
        throw new Error(`${errorPrefix} as first argument of isSameOrBefore method`);
    }

    isBetween(d1, d2, unit) {
        const validDate1 = Datestamp.getValid(d1);
        const validDate2 = Datestamp.getValid(d2);
        if (validDate1 && validDate2) {
            return diff.isBetween(this.date, validDate1.date, validDate2.date, unit, firstDayOfWeek);
        }
        throw new Error(`${errorPrefix} as first and second argument of isBetween method`);
    }

    format(str) {
        return format(this, str);
    }
}


const datestamp = date => new Datestamp(date);
datestamp.firstDayOfWeek = num => { firstDayOfWeek = num; };

export default datestamp;
