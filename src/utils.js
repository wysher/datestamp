import { UNITS, BASIC_UNITS } from './constans';
import { getDiff } from './diff';

export const isObject = a => (!!a) && (a.constructor === Object);
export const isDefined = a => a !== undefined;
export const isDate = a => a instanceof Date;
export const clone = d => d ? new Date(+d) : undefined;

export const normalizeUnit = unit => {
    const normalizedUnit = typeof unit === 'string' && unit.length > 1 ? unit.toLowerCase() : unit;
    const foundUnit = UNITS.find(({ aliases }) => aliases.some(alias => alias === normalizedUnit));
    return foundUnit ? foundUnit.name : undefined;
};

export const normalizeUnitsObject = obj => (
    Object
        .entries(obj)
        .reduce((memo, [key, value]) => ({ ...memo, [normalizeUnit(key)]: value }), {})
);

export const setDateFromObject = (d, obj) => {
    const normalizedObj = normalizeUnitsObject(obj);
    UNITS.forEach(({ name, set }) => {
        const unit = normalizedObj[name];
        if (isDefined(unit)) {
            const count = name === 'month' ? 1 : 0; // damn months starts with 0
            d[set](unit - count);
        }
    });
    return d;
};

export const constructDate = (val) => {
    if (isDefined(val)) {
        if (isDate(val)) {
            return new Date(val);
        } else if (Array.isArray(val)) {
            // from array
            const arr = [...val];
            arr[1] -= 1; // month starts with 0
            return new Date(...arr);
        } else if (isObject(val)) {
            // from object
            const date = new Date();
            return setDateFromObject(date, val);
        }
    }
    // when nothing passed
    return new Date();
};

export const getDayOfYear = (d) => {
    const diff = clone(d);
    diff.setMonth(0, 0);
    return Math.ceil((d - diff) / 8.64e7);
};

export const getWeekNumber = (d, firstDayOfWeek) => {
    const date = clone(d);
    date.setHours(0, 0, 0, 0);
    // set date to Thursday
    date.setDate((date.getDate() + 4) - (date.getDay() + firstDayOfWeek || 7));
    // it's a kind of magic
    return Math.ceil(((date - new Date(date.getFullYear(), 0, 1)) / (8.64e7 + 1)) / 7);
};

export const add = (d, obj) => {
    const normalizedObj = normalizeUnitsObject(obj);
    UNITS.forEach(({ name, set, get }) => {
        const multiplier = name === 'week' ? 7 : 1;
        const count = normalizedObj[name];
        if (isDefined(count)) {
            d[set](d[get]() + (count * multiplier));
        }
    });
    return d;
};

export const startOf = (d, unit = 'millisecond', firstDayOfWeek = 0) => {
    let normalizedUnit = normalizeUnit(unit);
    if (normalizedUnit === 'week') {
        const day = d.getDay();
        const diff = (day < firstDayOfWeek ? 7 : 0) + (day - firstDayOfWeek);

        d.setDate(d.getDate() - diff);
        normalizedUnit = 'day';
    }
    const arr = [...BASIC_UNITS].reverse();
    const index = arr.findIndex(({ name }) => name === normalizedUnit);
    arr
        .slice(index + 1)
        .forEach(({ set, name }) => {
            d[set](name === 'day' ? 1 : 0);
        });
    return d;
};

export const endOf = (d, unit = 'millisecond', firstDayOfWeek) => {
    const date = add(d, { [unit]: 1 });
    startOf(date, unit, firstDayOfWeek);
    date.setMilliseconds(-1);
    return date;
};

export const getWeekday = (date, firstDayOfWeek) => {
    const diff = startOf(clone(date), 'week', firstDayOfWeek);
    return Math.floor(getDiff(diff, date, 'day')) + 1;
};
