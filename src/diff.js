import { normalizeUnit, startOf } from './utils';

const getMonthDiff = (d1, d2) => {
    const d1Y = d1.getFullYear();
    const d2Y = d2.getFullYear();
    const d1M = d1.getMonth();
    const d2M = d2.getMonth();
    return ((d2Y - d1Y) * 12) + (d2M - d1M);
};

const cloneWithOffset = (d1, d2) => {
    // need to consider DST
    const timeOffset = d1.getTimezoneOffset() - d2.getTimezoneOffset();
    const date = new Date(d1);
    date.setMinutes(date.getMinutes() - timeOffset);
    return date;
};

/* eslint-disable complexity */
export const getDiff = (d1, d2, unit = 'ms') => {
    const normalizedUnit = normalizeUnit(unit);
    const t1 = cloneWithOffset(d1, d2);
    const t2 = d2;

    let diff = 0;
    switch (normalizedUnit) {
    case 'year': diff = getMonthDiff(t1, t2) / 12; break;
    case 'month': diff = getMonthDiff(t1, t2); break;
    case 'week': diff = (t2 - t1) / 6048e5; break; // 1000 * 60 * 60 * 24 * 7
    case 'day': diff = (t2 - t1) / 864e5; break; // 1000 * 60 * 60 * 24
    case 'hour': diff = (t2 - t1) / 36e5; break; // 1000 * 60 * 60
    case 'minute': diff = (t2 - t1) / 6e4; break; // 1000 * 60
    case 'second': diff = (t2 - t1) / 1e3; break; // 1000
    default: diff = (t2 - t1);
    }
    return diff;
};

// fdow = firstDayOfWeek
export const isAfter = (d1, d2, unit, fdow) => +startOf(d1, unit, fdow) > +startOf(d2, unit, fdow);
export const isBefore = (d1, d2, unit, fdow) => +startOf(d1, unit, fdow) < +startOf(d2, unit, fdow);
export const isSame = (d1, d2, unit, fdow) => +startOf(d1, unit, fdow) === +startOf(d2, unit, fdow);
export const isSameOrAfter = (d1, d2, unit, fdow) => +startOf(d1, unit, fdow) >= +startOf(d2, unit, fdow);
export const isSameOrBefore = (d1, d2, unit, fdow) => +startOf(d1, unit, fdow) <= +startOf(d2, unit, fdow);
export const isBetween = (date, d1, d2, unit, fdow) => {
    const testDate = +startOf(date, unit, fdow);
    const a = +startOf(d1, unit, fdow);
    const b = +startOf(d2, unit, fdow);
    return (testDate > a && testDate < b) || (testDate > b && testDate < a);
};
