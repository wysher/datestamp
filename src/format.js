const pad = (str, length = 2) => String(str).padStart(length, 0);

export const getReplaceMap = ({
    second,
    minute,
    hour,
    day,
    month,
    year,
}) => {
    const hours12 = hour % 12 === 0 ? 12 : hour % 12;
    return {
        YYYY: year,
        YY: String(year).slice(-2),
        Y: year,
        MM: pad(month),
        M: month,
        DD: pad(day),
        D: day,
        dd: pad(day),
        d: day,
        HH: pad(hour),
        H: hour,
        hh: pad(hours12 % 12),
        h: hours12,
        mm: pad(minute),
        m: minute,
        SS: pad(second),
        S: second,
        ss: pad(second),
        s: second,
        PP: (hour >= 12) ? 'PM' : 'AM',
        pp: (hour >= 12) ? 'pm' : 'am',
    };
};

const format = (obj, str) => {
    const { date, ...rest } = obj;
    if (str && typeof str === 'string') {
        const normalizedStr = str.toLowerCase();
        if (normalizedStr === 'date') return date.toLocaleDateString();
        if (normalizedStr === 'time') return date.toLocaleTimeString();
        const replaceMap = getReplaceMap(rest);
        let resultStr = str;
        Object
            .entries(replaceMap)
            .forEach(([key, value]) => {
                resultStr = resultStr.replace(key, value);
            });
        return resultStr;
    }
    return date.toLocaleString();
};

export default format;
