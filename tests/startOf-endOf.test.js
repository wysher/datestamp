import datestamp from 'src/Datestamp';
import { basicAliases, allAliases } from './testHelpers';

describe('Datestamp startOf / endOf methods', () => {
    const arr = [2018, 5, 9, 8, 7, 6, 5];
    const [year, month, day, hour, minute, second, millisecond] = arr;
    const firstDayOfWeek = 1;
    const lastDayOfWeek = 7;

    const obj = {
        year,
        month,
        day,
        hour,
        minute,
        second,
        millisecond,
    };
    const keys = Object.keys(obj).map(key => key);

    const startOfMap = {
        year,
        month: 1,
        day: 1,
        hour: 0,
        minute: 0,
        second: 0,
        millisecond: 0,
    };

    const endOfMap = {
        year,
        month: 12,
        day: 31,
        hour: 23,
        minute: 59,
        second: 59,
        millisecond: 999,
    };

    let a;
    beforeEach(() => {
        a = datestamp(arr);
    });

    const getNextObj = (map, slicedArr) => slicedArr.reduce((memo, current) => ({
        ...memo,
        [current]: map[current],
    }), {});

    describe('startOf, endOf basic units', () => {
        const aliases = basicAliases.filter(({ name }) => name !== 'millisecond');

        aliases.forEach(({ alias, name }) => {
            const index = keys.findIndex(elem => elem === name);
            const slicedUnits = keys.slice(index + 1);

            it(`should return proper object when used startOf method with ${alias} alias`, () => {
                const b = a.startOf(alias);
                expect(b).toEqual(expect.objectContaining({
                    ...obj,
                    ...getNextObj(startOfMap, slicedUnits),
                }));
                expect(a === b).toBe(false);
            });

            it(`should return proper object when used endOf method with ${alias} alias`, () => {
                const b = a.endOf(alias);
                expect(b).toEqual(expect.objectContaining({
                    ...obj,
                    ...getNextObj(endOfMap, slicedUnits),
                }));
                expect(a === b).toBe(false);
            });
        });
    })

    describe('startOf / endOf week', () =>{
        const weekAliases = allAliases.filter(({ name }) => name === 'week');
        weekAliases.forEach(({ alias }) => {
            const index = keys.findIndex(elem => elem === 'day');
            const slicedUnits = keys.slice(index + 1);
            it(`should return proper object when used startOf method with ${alias} alias`, () => {
                const b = a.startOf(alias);
                const diff = day - a.date.getDay();
                expect(b).toEqual(expect.objectContaining({
                    ...obj,
                    ...getNextObj(startOfMap, slicedUnits),
                    day: diff,
                }));
                expect(b.weekday()).toEqual(firstDayOfWeek);
                expect(a === b).toBe(false);
            });

            it(`should return proper object when used endOf method with ${alias} alias`, () => {
                const b = a.endOf(alias);
                const diff = day + (lastDayOfWeek - a.weekday());
                expect(b).toEqual(expect.objectContaining({
                    ...obj,
                    ...getNextObj(endOfMap, slicedUnits),
                    day: diff,
                }));
                expect(b.weekday()).toEqual(lastDayOfWeek);
                expect(a === b).toBe(false);
            });
        });
    });
});
