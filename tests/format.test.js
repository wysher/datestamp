import datestamp from 'src/Datestamp';

describe('Datestamp format method', () => {
    const obj = {
        Y: 2018,
        M: 1,
        D: 2,
        H: 3,
        m: 4,
        s: 5,
    };
    let a;
    beforeEach(() => {
        a = datestamp(obj);
    });

    it('should return full year', () => {
        const { Y } = obj;
        expect(a.format('YYYY')).toBe(String(Y));
        expect(a.format('Y')).toBe(String(Y));
    });
    it('should return last two diggits of year', () => {
        expect(a.format('YY')).toBe(String(obj.Y).slice(-2));
    });

    it('should return month', () => {
        expect(a.format('M')).toBe(String(obj.M));
    });
    it('should return padded month', () => {
        expect(a.format('MM')).toBe(`0${obj.M}`);
    });

    it('should return day', () => {
        const { D } = obj;
        expect(a.format('D')).toBe(String(D));
        expect(a.format('d')).toBe(String(D));
    });
    it('should return padded day', () => {
        const { D } = obj;
        expect(a.format('DD')).toBe(`0${D}`);
        expect(a.format('dd')).toBe(`0${D}`);
    });

    it('should return hour', () => {
        const { H } = obj;
        expect(a.format('H')).toBe(String(H));
        expect(a.format('h')).toBe(String(H));
    });
    it('should return padded hour', () => {
        const { H } = obj;
        expect(a.format('HH')).toBe(`0${H}`);
        expect(a.format('hh')).toBe(`0${H}`);
    });

    it('should return minute', () => {
        const { m } = obj;
        expect(a.format('m')).toBe(String(m));
    });
    it('should return padded minute', () => {
        const { m } = obj;
        expect(a.format('mm')).toBe(`0${m}`);
    });

    it('should return second', () => {
        const { s } = obj;
        expect(a.format('S')).toBe(String(s));
        expect(a.format('s')).toBe(String(s));
    });
    it('should return padded second', () => {
        const { s } = obj;
        expect(a.format('SS')).toBe(`0${s}`);
        expect(a.format('ss')).toBe(`0${s}`);
    });

    it('should return AM suffix', () => {
        expect(a.format('PP')).toBe('AM');
    });
    it('should return am suffix', () => {
        expect(a.format('pp')).toBe('am');
    });

    it('should return string untouched', () => {
        const str = '`1234567890-=~!@#$%^&*()_+[]\\{}|:\';",./<>? ';

        expect(a.format(str)).toBe(str);
    });

    it('should return properly formatted string', () => {
        // eslint-disable-next-line
        const { D, M, Y, H, m, s } = obj;

        expect(a.format('DD.MM.YYYY HH:mm:ss'))
            .toBe(`0${D}.0${M}.${Y} 0${H}:0${m}:0${s}`);

        expect(a.format('YYYY-MM-DD'))
            .toBe(`${Y}-0${M}-0${D}`);
    });
});
