import { UNITS, BASIC_UNITS } from 'src/constans';

const getAliases = arr => arr.reduce((all, current) => {
    const namedAliases = current.aliases.reduce((memo, alias) => [
        ...memo,
        {
            name: current.name,
            alias,
        },
    ], [])
    return ([
        ...all,
        ...namedAliases,
    ]);
}, []);

export const basicAliases = getAliases(BASIC_UNITS);
export const allAliases = getAliases(UNITS);
