import datestamp from 'src/Datestamp';
import { basicAliases } from './testHelpers';

describe('Datestamp set method', () => {
    const arr = [2018, 10, 9, 8, 7, 6, 5];
    const [year, month, day, hour, minute, second, millisecond] = arr;

    const obj = {
        year,
        month,
        day,
        hour,
        minute,
        second,
        millisecond,
    };

    let a;
    beforeEach(() => {
        a = datestamp(arr);
    });

    basicAliases.forEach(({ name, alias }) => {
        const num = name === year ? 2000 : 1;

        it(`should set properly with string and value as arguments using ${alias} alias`, () => {
            const b = a.set(alias, num);
            expect(a).toEqual(expect.objectContaining(obj));
            expect(b).toEqual(expect.objectContaining({
                ...obj,
                [name]: num,
            }));
            expect(a === b).toBe(false);
        });

        it(`should set properly using object with ${alias} alias`, () => {
            const b = a.set({ [alias]: num });
            expect(b).toEqual(expect.objectContaining({
                ...obj,
                [name]: num,
            }));
            expect(a === b).toBe(false);
        });
    });

    it('should set properly using object with multiple values', () => {
        const values = {
            year: 2001,
            month: 1,
            day: 2,
            hour: 3,
            minute: 4,
            second: 5,
            millisecond: 6,
        }
        const b = a.set(values);
        expect(b).toEqual(expect.objectContaining(values));
        expect(a === b).toBe(false);
    });
});
