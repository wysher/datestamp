import datestamp from 'src/Datestamp';
import { BASIC_UNITS } from 'src/constans';

describe('Datestamp add, subtract methods', () => {
    const arr = [2018, 10, 9, 8, 7, 6, 5];
    const [year, month, day, hour, minute, second, millisecond] = arr;

    const obj = {
        year,
        month,
        day,
        hour,
        minute,
        second,
        millisecond,
    };

    let a;
    beforeEach(() => {
        a = datestamp(arr);
    });

    describe('add / subtract with aliases', () => {
        Object.keys(obj).forEach(key => {
            const currentAliases = BASIC_UNITS.reduce((memo, { name, aliases }) => {
                if (key === name) memo.push(...aliases);
                return memo;
            }, []);

            currentAliases.forEach(alias => {
                it(`should add 1 ${key} using ${alias} alias and return new datestamp object`, () => {
                    const b = a.add(1, alias);
                    expect(a).toEqual(expect.objectContaining(obj));
                    expect(b).toEqual(expect.objectContaining({
                        ...obj,
                        [key]: obj[key] + 1,
                    }));
                    expect(a === b).toBe(false);
                });

                it(`should add 1 ${key} using ${alias} alias with object and return new datestamp object`, () => {
                    const b = a.add({ [alias]: 1 });
                    expect(a).toEqual(expect.objectContaining(obj));
                    expect(b).toEqual(expect.objectContaining({
                        ...obj,
                        [key]: obj[key] + 1,
                    }));
                    expect(a === b).toBe(false);
                });

                it(`should subtract 1 ${key} using ${alias} alias and return new datestamp object`, () => {
                    const b = a.subtract(1, alias);
                    expect(a).toEqual(expect.objectContaining(obj));
                    expect(b).toEqual(expect.objectContaining({
                        ...obj,
                        [key]: obj[key] - 1,
                    }));
                    expect(a === b).toBe(false);
                });

                it(`should subtract 1 ${key} using ${alias} alias with object and return new datestamp object`, () => {
                    const b = a.subtract({ [alias]: 1 });
                    expect(a).toEqual(expect.objectContaining(obj));
                    expect(b).toEqual(expect.objectContaining({
                        ...obj,
                        [key]: obj[key] - 1,
                    }));
                    expect(a === b).toBe(false);
                });
            });
        });
    });

    describe('add / subtract with remainders', () => {
        BASIC_UNITS.forEach(({ name, set, get }) => {
            const num = 12000;

            it(`should add ${num} ${name}s`, () => {
                const b = a.add(num, name);
                const expected = new Date(a.unix);
                expected[set](expected[get]() + num);
                expect(b.unix).toBe(+expected);
                expect(a === b).toBe(false);
            });

            it(`should subtract ${num} ${name}s`, () => {
                const b = a.subtract(num, name);
                const expected = new Date(a.unix);
                expected[set](expected[get]() - num);
                expect(b.unix).toBe(+expected);
                expect(a === b).toBe(false);
            });
        });
    });

    describe('add / subtract using object with multiple values', () => {
        const num = 1;
        const values = {
            year: num,
            month: num,
            day: num,
            hour: num,
            minute: num,
            second: num,
            millisecond: num,
        };

        it('should add properly multiple values', () => {
            const b = a.add(values);
            expect(b).toEqual(expect.objectContaining({
                year: year + num,
                month: month + num,
                day: day + num,
                hour: hour + num,
                minute: minute + num,
                second: second + num,
                millisecond: millisecond + num,
            }));
            expect(a === b).toBe(false);
        });

        it('should subtract properly multiple values', () => {
            const b = a.subtract(values);
            expect(b).toEqual(expect.objectContaining({
                year: year - num,
                month: month - num,
                day: day - num,
                hour: hour - num,
                minute: minute - num,
                second: second - num,
                millisecond: millisecond - num,
            }));
            expect(a === b).toBe(false);
        });
    });
});
