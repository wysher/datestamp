import datestamp from 'src/Datestamp';

// TODO add more test for is... methods for each unit with aliases

describe('Datestamp isAfter / isBefore / isSame / isSameOrBefore / isSameOrAfter / isBetween methods', () => {
    const str = 'Foo';
    let a;
    let dateObj;
    beforeEach(() => {
        a = datestamp([2017, 1, 15, 12, 30]);
        dateObj = new Date(2017, 1, 15, 12, 31);
    });

    describe('isAfter', () => {
        it('by hour, a=12:30 b=12:00 => false', () => {
            const b = datestamp([2017, 1, 15, 12, 0]);
            expect(a.isAfter(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=11:59 => true', () => {
            const b = datestamp([2017, 1, 15, 11, 59]);
            expect(a.isAfter(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=12:59 => false', () => {
            const b = datestamp([2017, 1, 15, 12, 59]);
            expect(a.isAfter(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=11:30 => true', () => {
            const b = datestamp([2017, 1, 15, 11, 30]);
            expect(a.isAfter(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=13:00 => false', () => {
            const b = datestamp([2017, 1, 15, 13]);
            expect(a.isAfter(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,nextDay => false', () => {
            const b = datestamp([2017, 1, 16, 12, 30]);
            expect(a.isAfter(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,nextMonth => false', () => {
            const b = datestamp([2017, 2, 15, 12, 30]);
            expect(a.isAfter(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,nextYear => false', () => {
            const b = datestamp([2018, 1, 15, 12, 30]);
            expect(a.isAfter(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,prevDay => true', () => {
            const b = datestamp([2017, 1, 14, 12, 30]);
            expect(a.isAfter(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=12:30,prevMonth => true', () => {
            const b = datestamp([2017, 0, 15, 12, 30]);
            expect(a.isAfter(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=12:30,prevYear => true', () => {
            const b = datestamp([2016, 1, 15, 12, 30]);
            expect(a.isAfter(b, 'hour')).toBe(true);
        });
        it('should return proper value and not throw Error when passed Date object as first argument', () => {
            expect(a.isAfter(dateObj, 'hour')).toBe(false);
            expect(() => { a.isAfter(dateObj, 'hour'); }).not.toThrow();
        });
        it('should throw Error when passed argument is not Date or datestamp object', () => {
            expect(() => { a.isAfter(str, 'hour'); }).toThrow();
        });
    });

    describe('isBefore', () => {
        it('by hour, a=12:30 b=12:00 => false', () => {
            const b = datestamp([2017, 1, 15, 12, 0]);
            expect(a.isBefore(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=11:59 => false', () => {
            const b = datestamp([2017, 1, 15, 11, 59]);
            expect(a.isBefore(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:59 => false', () => {
            const b = datestamp([2017, 1, 15, 12, 59]);
            expect(a.isBefore(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=11:30 => false', () => {
            const b = datestamp([2017, 1, 15, 11, 30]);
            expect(a.isBefore(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=13:00 => true', () => {
            const b = datestamp([2017, 1, 15, 13]);
            expect(a.isBefore(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=12:30,nextDay => true', () => {
            const b = datestamp([2017, 1, 16, 12, 30]);
            expect(a.isBefore(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=12:30,nextMonth => true', () => {
            const b = datestamp([2017, 2, 15, 12, 30]);
            expect(a.isBefore(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=12:30,nextYear => true', () => {
            const b = datestamp([2018, 1, 15, 12, 30]);
            expect(a.isBefore(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=12:30,prevDay => false', () => {
            const b = datestamp([2017, 1, 14, 12, 30]);
            expect(a.isBefore(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,prevMonth => false', () => {
            const b = datestamp([2017, 0, 15, 12, 30]);
            expect(a.isBefore(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,prevYear => false', () => {
            const b = datestamp([2016, 1, 15, 12, 30]);
            expect(a.isBefore(b, 'hour')).toBe(false);
        });
        it('should return proper value and not throw Error when passed Date object as first argument', () => {
            expect(a.isBefore(dateObj, 'hour')).toBe(true);
            expect(() => { a.isBefore(dateObj, 'hour'); }).not.toThrow();
        });
        it('should throw Error when passed argument is not Date or datestamp object', () => {
            expect(() => { a.isBefore(str, 'hour'); }).toThrow();
        });
    });

    describe('isSame', () => {
        it('by hour, a=12:30 b=12:00 => true', () => {
            const b = datestamp([2017, 1, 15, 12, 0]);
            expect(a.isSame(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=11:59 => false', () => {
            const b = datestamp([2017, 1, 15, 11, 59]);
            expect(a.isSame(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:59 => true', () => {
            const b = datestamp([2017, 1, 15, 12, 59]);
            expect(a.isSame(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=11:30 => false', () => {
            const b = datestamp([2017, 1, 15, 11, 30]);
            expect(a.isSame(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=13:00 => false', () => {
            const b = datestamp([2017, 1, 15, 13]);
            expect(a.isSame(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,nextDay => false', () => {
            const b = datestamp([2017, 1, 16, 12, 30]);
            expect(a.isSame(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,nextMonth => false', () => {
            const b = datestamp([2017, 2, 15, 12, 30]);
            expect(a.isSame(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,nextYear => false', () => {
            const b = datestamp([2018, 1, 15, 12, 30]);
            expect(a.isSame(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,prevDay => false', () => {
            const b = datestamp([2017, 1, 14, 12, 30]);
            expect(a.isSame(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,prevMonth => false', () => {
            const b = datestamp([2017, 0, 15, 12, 30]);
            expect(a.isSame(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,prevYear => false', () => {
            const b = datestamp([2016, 1, 15, 12, 30]);
            expect(a.isSame(b, 'hour')).toBe(false);
        });
        it('should return proper value and not throw Error when passed Date object as first argument', () => {
            expect(a.isSame(dateObj, 'hour')).toBe(false);
            expect(() => { a.isSame(dateObj, 'hour'); }).not.toThrow();
        });
        it('should throw Error when passed argument is not Date or datestamp object', () => {
            expect(() => { a.isSame(str, 'hour'); }).toThrow();
        });
    });

    describe('isSameOrAfter', () => {
        it('by hour, a=12:30 b=12:00 => true', () => {
            const b = datestamp([2017, 1, 15, 12, 0]);
            expect(a.isSameOrAfter(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=11:59 => true', () => {
            const b = datestamp([2017, 1, 15, 11, 59]);
            expect(a.isSameOrAfter(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=12:59 => true', () => {
            const b = datestamp([2017, 1, 15, 12, 59]);
            expect(a.isSameOrAfter(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=11:30 => true', () => {
            const b = datestamp([2017, 1, 15, 11, 30]);
            expect(a.isSameOrAfter(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=13:00 => false', () => {
            const b = datestamp([2017, 1, 15, 13]);
            expect(a.isSameOrAfter(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,nextDay => false', () => {
            const b = datestamp([2017, 1, 16, 12, 30]);
            expect(a.isSameOrAfter(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,nextMonth => false', () => {
            const b = datestamp([2017, 2, 15, 12, 30]);
            expect(a.isSameOrAfter(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,nextYear => false', () => {
            const b = datestamp([2018, 1, 15, 12, 30]);
            expect(a.isSameOrAfter(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,prevDay => true', () => {
            const b = datestamp([2017, 1, 14, 12, 30]);
            expect(a.isSameOrAfter(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=12:30,prevMonth => true', () => {
            const b = datestamp([2017, 0, 15, 12, 30]);
            expect(a.isSameOrAfter(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=12:30,prevYear => true', () => {
            const b = datestamp([2016, 1, 15, 12, 30]);
            expect(a.isSameOrAfter(b, 'hour')).toBe(true);
        });
        it('should return proper value and not throw Error when passed Date object as first argument', () => {
            expect(a.isSameOrAfter(dateObj, 'hour')).toBe(false);
            expect(() => { a.isSameOrAfter(dateObj, 'hour'); }).not.toThrow();
        });
        it('should throw Error when passed argument is not Date or datestamp object', () => {
            expect(() => { a.isSameOrAfter(str, 'hour'); }).toThrow();
        });
    });

    describe('isSameOrBefore', () => {
        it('by hour, a=12:30 b=12:00 => true', () => {
            const b = datestamp([2017, 1, 15, 12, 0]);
            expect(a.isSameOrBefore(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=11:59 => false', () => {
            const b = datestamp([2017, 1, 15, 11, 59]);
            expect(a.isSameOrBefore(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:59 => true', () => {
            const b = datestamp([2017, 1, 15, 12, 59]);
            expect(a.isSameOrBefore(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=11:30 => false', () => {
            const b = datestamp([2017, 1, 15, 11, 30]);
            expect(a.isSameOrBefore(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=13:00 => true', () => {
            const b = datestamp([2017, 1, 15, 13]);
            expect(a.isSameOrBefore(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=12:30,nextDay => true', () => {
            const b = datestamp([2017, 1, 16, 12, 30]);
            expect(a.isSameOrBefore(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=12:30,nextMonth => true', () => {
            const b = datestamp([2017, 2, 15, 12, 30]);
            expect(a.isSameOrBefore(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=12:30,nextYear => true', () => {
            const b = datestamp([2018, 1, 15, 12, 30]);
            expect(a.isSameOrBefore(b, 'hour')).toBe(true);
        });
        it('by hour, a=12:30 b=12:30,prevDay => false', () => {
            const b = datestamp([2017, 1, 14, 12, 30]);
            expect(a.isSameOrBefore(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,prevMonth => false', () => {
            const b = datestamp([2017, 0, 15, 12, 30]);
            expect(a.isSameOrBefore(b, 'hour')).toBe(false);
        });
        it('by hour, a=12:30 b=12:30,prevYear => false', () => {
            const b = datestamp([2016, 1, 15, 12, 30]);
            expect(a.isSameOrBefore(b, 'hour')).toBe(false);
        });
        it('should return proper value and not throw Error when passed Date object as first argument', () => {
            expect(a.isSameOrBefore(dateObj, 'hour')).toBe(true);
            expect(() => { a.isSameOrBefore(dateObj, 'hour'); }).not.toThrow();
        });
        it('should throw Error when passed argument is not Date or datestamp object', () => {
            expect(() => { a.isSameOrBefore(str, 'hour'); }).toThrow();
        });
    });


    describe('isBetween', () => {
        // b is before c is after => true
        it('by hour, a=11:59, c=13:00', () => {
            const b = datestamp([2017, 1, 15, 11, 59]);
            const c = datestamp([2017, 1, 15, 13, 0]);
            expect(a.isBetween(b, c, 'hour')).toBe(true);
        });
        it('by hour, a=11:59, c=12:30,nextDay', () => {
            const b = datestamp([2017, 1, 15, 11, 59]);
            const c = datestamp([2017, 1, 16, 12, 30]);
            expect(a.isBetween(b, c, 'hour')).toBe(true);
        });
        it('by hour, a=12:30,prevDay, c=13:00', () => {
            const b = datestamp([2017, 1, 14, 12, 30]);
            const c = datestamp([2017, 1, 15, 13, 0]);
            expect(a.isBetween(b, c, 'hour')).toBe(true);
        });
        it('by hour, a=12:30,prevMonth, c=12:30,nextMonth', () => {
            const b = datestamp([2017, 0, 15, 12, 30]);
            const c = datestamp([2017, 2, 15, 12, 30]);
            expect(a.isBetween(b, c, 'hour')).toBe(true);
        });
        // b is before c is same => false
        it('by hour, a=11:59, c=12:59', () => {
            const b = datestamp([2017, 1, 15, 11, 59]);
            const c = datestamp([2017, 1, 15, 12, 59]);
            expect(a.isBetween(b, c, 'hour')).toBe(false);
        });
        // b is before c is before => false
        it('by hour, a=11:59, c=11:59', () => {
            const b = datestamp([2017, 1, 15, 11, 59]);
            const c = datestamp([2017, 1, 15, 11, 59]);
            expect(a.isBetween(b, c, 'hour')).toBe(false);
        });
        // b is after c is before => true
        it('by hour, a=13:00, c=11:59', () => {
            const b = datestamp([2017, 1, 15, 13, 0]);
            const c = datestamp([2017, 1, 15, 11, 59]);
            expect(a.isBetween(b, c, 'hour')).toBe(true);
        });
        it('by hour, a=12:30,nextDay, c=11:59', () => {
            const b = datestamp([2017, 1, 16, 12, 30]);
            const c = datestamp([2017, 1, 15, 11, 59]);
            expect(a.isBetween(b, c, 'hour')).toBe(true);
        });
        it('by hour, a13:00=, c=12:30,prevDay', () => {
            const b = datestamp([2017, 1, 15, 13, 0]);
            const c = datestamp([2017, 1, 14, 12, 30]);
            expect(a.isBetween(b, c, 'hour')).toBe(true);
        });
        it('by hour, a=12:30,nextMonth, c=12:30,prevMonth', () => {
            const b = datestamp([2017, 2, 15, 12, 30]);
            const c = datestamp([2017, 0, 15, 12, 30]);
            expect(a.isBetween(b, c, 'hour')).toBe(true);
        });
        // b is after c is same => false
        it('by hour, a=13:00, c=12:00', () => {
            const b = datestamp([2017, 1, 15, 13, 0]);
            const c = datestamp([2017, 1, 15, 12, 0]);
            expect(a.isBetween(b, c, 'hour')).toBe(false);
        });
        // b is after c is after => false
        it('by hour, a=13:00, c=13:00', () => {
            const b = datestamp([2017, 1, 15, 13, 0]);
            const c = datestamp([2017, 1, 15, 13, 0]);
            expect(a.isBetween(b, c, 'hour')).toBe(false);
        });
        it('should return proper value and not throw Error when passed Date object as first argument', () => {
            const dt = a.add(1, 'hour');
            expect(a.isBetween(dateObj, dt, 'hour')).toBe(false);
            expect(() => { a.isBetween(dateObj, dt, 'hour'); }).not.toThrow();
        });
        it('should return proper value and not throw Error when passed Date object as second argument', () => {
            const dt = a.subtract(1, 'hour');
            expect(a.isBetween(dt, dateObj, 'hour')).toBe(true);
            expect(() => { a.isBetween(dt, dateObj, 'hour'); }).not.toThrow();
        });
        it('should throw Error when passed arguments are not Date or datestamp object', () => {
            const dt = datestamp([2017, 1, 15, 13, 0]);
            expect(() => { a.isBetween(str, dt, 'hour'); }).toThrow();
            expect(() => { a.isBetween(dt, str, 'hour'); }).toThrow();
        });
    });

    describe('min', () => {
        it('should return min properly when passed datestamp object', () => {
            const b = a.add(1, 'ms');
            expect(a.min(b)).toEqual(a);
            expect(b.min(a)).toEqual(a);
            expect(() => { a.min(b); }).not.toThrow();
        });
        it('should return min properly when passed Date object', () => {
            const b = new Date();
            expect(a.min(b)).toEqual(a);
            expect(() => { a.min(b); }).not.toThrow();
        });
        it('should throw and Error when not Date or datestamp object passed', () => {
            expect(() => { a.min(str); }).toThrow();
        });
    });

    describe('max', () => {
        it('should return max properly when passed datestamp object', () => {
            const b = a.subtract(1, 'ms');
            expect(a.max(b)).toEqual(a);
            expect(b.max(a)).toEqual(a);
            expect(() => { a.max(b); }).not.toThrow();
        });
        it('should return max properly when passed Date object', () => {
            const b = new Date();
            expect(a.max(b)).toEqual(datestamp(b));
            expect(() => { a.max(b); }).not.toThrow();
        });
        it('should throw and Error when not Date or datestamp object passed', () => {
            expect(() => { a.max(str); }).toThrow();
        });
    });
});
