import datestamp from 'src/Datestamp';
import { allAliases } from './testHelpers';

describe('Datestamp diff method', () => {
    describe('construct from object with aliases', () => {
        allAliases.forEach(({ name, alias }) => {
            it(`should return proper diff for ${name} unit using ${alias} alias`, () => {
                // value should be big enough, but not to big to consider DST for all the possible tests runs
                const value = 200;
                const a = datestamp();
                const b = a.add({ [name]: value });

                expect(a.diff(b, alias)).toEqual(value);
                expect(b.diff(a, alias)).toEqual(-value);
            });
        });
    });
});
