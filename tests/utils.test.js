import datestamp, { Datestamp } from 'src/datestamp';
import {
    isObject,
    isDefined,
    isDate,
    clone,
    normalizeUnit,
    normalizeUnitsObject,
} from 'src/utils';
import { UNITS } from 'src/constans';

describe('utils', () => {
    const TYPES = [
        {},
        new Date(),
        undefined,
        [],
        'foo',
        123,
        true,
        false,
        null,
        // eslint-disable-next-line
        Symbol(),
    ];
    describe('helpers', () => {
        describe('isObject', () => {
            it('should return true when passed object', () => {
                expect(isObject({})).toBe(true);
            });
            it('should return false when passed other type', () => {
                TYPES.slice(1).forEach((type) => {
                    expect(isObject(type)).toBe(false);
                });
            });
        });

        describe('isDefined', () => {
            it('should return false if is undefined', () => {
                expect(isDefined()).toBe(false);
            });
            it('should return false when passed undefined', () => {
                expect(isDefined(undefined)).toBe(false);
            });
            it('should return true when passed undefined as string', () => {
                expect(isDefined('undefined')).toBe(true);
            });
            it('should return true when passed any type', () => {
                TYPES
                    .filter(type => type !== undefined)
                    .forEach(type => {
                        expect(isDefined(type)).toBe(true);
                    });
            });
        });

        describe('isDate', () => {
            it('should return true when passed Date object', () => {
                expect(isDate(new Date())).toBe(true);
            });
            it('should return false when passed any other type then Date', () => {
                TYPES
                    .filter(type => !(type instanceof Date))
                    .forEach(type => {
                        expect(isDate(type)).toBe(false);
                    });
            });
        });

        describe('clone', () => {
            it('should return cloned date', () => {
                const a = new Date();
                const b = clone(a);
                // compare valueOf
                expect(+a === +b).toBe(true);
                // check reference
                expect(a === b).toBe(false);
            });
            it('should return undefined when called without args', () => {
                expect(clone()).toBe(undefined);
            });
        });
    });

    describe('normalize', () => {
        describe('normalizeUnit', () => {
            // all units in array
            UNITS.forEach(unit => {
                unit.aliases.forEach(alias => {
                    it(`should return ${unit.name} for '${alias}' alias`, () => {
                        expect(normalizeUnit(alias)).toBe(unit.name);
                    });
                });
            });
            // aliases with length < 1 should be 'lowercased'
            UNITS.forEach(unit => {
                unit.aliases
                    .filter(alias => alias.length < 1)
                    .map(alias => alias.toUpperCase())
                    .forEach(alias => {
                        it(`should return ${unit.name} for 'uppercased' '${alias}' alias`, () => {
                            expect(normalizeUnit(alias)).toBe(unit.name);
                        });
                    });
            });
        });

        describe('normalizeUnitsObject', () => {
            UNITS.forEach(unit => {
                unit.aliases.forEach(alias => {
                    it(`should return object with normalized ${unit.name} for '${alias}' alias`, () => {
                        expect(normalizeUnitsObject({ [alias]: 1 }))
                            .toEqual({ [unit.name]: 1 });
                    });
                });
            });

            it('should return normalized object when passed down has more than one key', () => {
                expect(normalizeUnitsObject({
                    ms: 1,
                    hr: 2,
                    d: 3,
                })).toEqual({
                    millisecond: 1,
                    hour: 2,
                    day: 3,
                });
            });
        });
    });

    describe('Datestamp static getValid', () => {
        it('should return instance of Datestamp when passed datestamp object', () => {
            const a = datestamp();
            expect(Datestamp.getValid(a)).toBeInstanceOf(Datestamp);
        });

        it('should return instance of Datestamp when passed Date object', () => {
            const a = new Date();
            expect(Datestamp.getValid(a)).toBeInstanceOf(Datestamp);
        });

        it('should return null when passed any type other than datestamp or Date', () => {
            TYPES.filter(type => !(type instanceof Date)).forEach(type => {
                const a = type;
                expect(Datestamp.getValid(a)).toBe(null);
            });
        });
    });
});
