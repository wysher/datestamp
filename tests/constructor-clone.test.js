import datestamp from 'src/Datestamp';
import { basicAliases } from './testHelpers';

describe('Datestamp constructor / clone methods', () => {
    const arr = [2018, 10, 9, 8, 7, 6, 5];
    const [year, month, day, hour, minute, second, millisecond] = arr;

    const dateArr = [...arr];
    dateArr[1] -= 1; // month starts with 0 within Date obj
    const dateObj = new Date(...dateArr);

    const obj = {
        year,
        month,
        day,
        hour,
        minute,
        second,
        millisecond,
    };

    const expectedObj = {
        ...obj,
        date: new Date(+dateObj),
        unix: +dateObj,
    };

    it('should construct properly from Array', () => {
        const dt = datestamp(arr);
        expect(dt).toEqual(expect.objectContaining(expectedObj));
    });

    it('should construct properly from Date', () => {
        const dt = datestamp(dateObj);
        expect(dt).toEqual(expect.objectContaining(expectedObj));
    });

    it('should construct properly when nothing passed', () => {
        const now = new Date();
        const dt = datestamp();
        expect(dt.date).toEqual(now);
    });

    it('should construct properly from object with multiple values', () => {
        const dt = datestamp({
            year,
            month,
            day,
            hour,
            minute,
            second,
            millisecond,
        });
        expect(dt).toEqual(expect.objectContaining(expectedObj));
    });

    describe('construct from object with aliases', () => {
        basicAliases.forEach(({ name, alias }) => {
            it(`should construct properly from object using ${alias} alias`, () => {
                const dt = datestamp({ [alias]: obj[name] });
                const now = new Date();

                const expected = {
                    year: now.getFullYear(),
                    month: now.getMonth() + 1,
                    day: now.getDate(),
                    hour: now.getHours(),
                    minute: now.getMinutes(),
                    second: now.getSeconds(),
                    // no need to test millisecond when not passed as alias
                    // especialy that sometimes now comes from next tick
                    // causing incorrect results
                    // but millisecond will be tested in cases when passed as alias
                };

                expect(dt).toEqual(expect.objectContaining({
                    ...expected,
                    [name]: obj[name],
                }));
            });
        });
    });

    describe('test construct for every day of whole year', () => {
        const arrayOfDays = Array.from({ length: 365 }, (_value, index) => index + 1);
        // we assume that used methods works properly`
        const dt = datestamp().startOf('year');
        arrayOfDays.forEach(dayNumber => {
            it(`should return proper datestamp object for ${dayNumber} day of year`, () => {
                const testDate = dt.set({ day: dayNumber });
                expect(testDate.weekday()).toEqual(testDate.date.getDay() + 1);
                // // first week of next year contains 31 december 2018
                expect(testDate.week()).toEqual(dayNumber === 365 ? 1 : Math.ceil(dayNumber / 7));
                expect(testDate.dayOfYear()).toEqual(dayNumber);
            });
        });
    });

    describe('weekday', () => {
        it('should return week and weekday properly when firstDayOfWeek is 0', () => {
            const saturday = datestamp({ year: 2018, month: 1, day: 6 });
            const sunday = saturday.add(1, 'day');
            const monday = sunday.add(1, 'day');
            expect(saturday.weekday()).toBe(7);
            expect(sunday.weekday()).toBe(1);
            expect(monday.weekday()).toBe(2);

            expect(saturday.week()).toBe(1);
            expect(sunday.week()).toBe(1);
            expect(monday.week()).toBe(2);
        });

        it('should return weekand  weekday properly when firstDayOfWeek is 1', () => {
            datestamp.firstDayOfWeek(1);
            const saturday = datestamp({ year: 2018, month: 1, day: 6 });
            const sunday = saturday.add(1, 'day');
            const monday = sunday.add(1, 'day');
            expect(saturday.weekday()).toBe(6);
            expect(sunday.weekday()).toBe(7);
            expect(monday.weekday()).toBe(1);

            expect(saturday.week()).toBe(1);
            expect(sunday.week()).toBe(2);
            expect(monday.week()).toBe(2);

            // reset global firstDayOfWeek
            datestamp.firstDayOfWeek(0);
        });
    });

    it('should clone datestamp object', () => {
        const a = datestamp(arr);
        expect(a).toBe(a);
        const b = a.clone();
        expect(a).toEqual(expect.objectContaining(obj));
        expect(b).toEqual(expect.objectContaining(obj));
        expect(a === b).toBe(false);
    });
});
