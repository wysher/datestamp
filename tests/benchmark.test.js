import datestamp from 'src/datestamp';
import moment from 'moment';
import { LocalDate } from 'js-joda';
import { DateTime } from 'luxon';

describe('benchmarks', () => {
    const times = 10000;
    const results = {};

    afterAll(() => {
        console.log('benchamrk results of creating objects', results);
    });

    it('benchmark for returning datestamp', () => {
        const start = performance.now();
        let dt;
        for (let i = 0; i < times; i += 1) {
            dt = datestamp();
        }
        const end = performance.now();
        results.datestamp = end - start;
    });

    it('benchmark for returning moment', () => {
        const start = performance.now();
        let dt;
        for (let i = 0; i < times; i += 1) {
            dt = moment();
        }
        const end = performance.now();
        results.moment = end - start;
    });

    it('benchmark for returning js Date object', () => {
        const start = performance.now();
        let dt;
        for (let i = 0; i < times; i += 1) {
            dt = new Date();
            dt.getMilliseconds();
            dt.getSeconds();
            dt.getMinutes();
            dt.getHours();
            dt.getDate();
            dt.getDay();
            dt.getMonth();
            dt.getYear();
        }
        const end = performance.now();
        results.Date = end - start;
    });

    it('benchmark for returning luxon', () => {
        const start = performance.now();
        let dt;
        for (let i = 0; i < times; i += 1) {
            dt = DateTime.local();
        }
        const end = performance.now();
        results.luxon = end - start;
    });

    it('benchmark for returning js joda', () => {
        const start = performance.now();
        let dt;
        for (let i = 0; i < times; i += 1) {
            dt = LocalDate.now();
        }
        const end = performance.now();
        results.jsJoda = end - start;
    });
});
